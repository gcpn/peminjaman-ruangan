const rooms = [
  {
    id: 0,
    kelas: "Anggrek",
    pendingin: true,
    proyektor: 1,
    kapasitas: 15,
    ukuran: "21m × 15m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/c/c3/MeetingRoomNutrifood.png"
  },
  {
    id: 1,
    kelas: "Anggrek",
    pendingin: true,
    proyektor: 0,
    kapasitas: 150,
    ukuran: "5m × 4m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/4/4f/Conferenceroom2.JPG"
  },
  {
    id: 2,
    kelas: "Anggrek",
    pendingin: true,
    proyektor: 2,
    kapasitas: 55,
    ukuran: "32m × 64m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Sitzungszimmer_SVP.jpg/640px-Sitzungszimmer_SVP.jpg"
  },
  {
    id: 3,
    kelas: "Anggrek",
    pendingin: true,
    proyektor: 1,
    kapasitas: 10,
    ukuran: "10m × 10m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Moseley_Hall_lounge.jpg/640px-Moseley_Hall_lounge.jpg"
  },
  {
    id: 4,
    kelas: "Melati",
    pendingin: true,
    proyektor: 2,
    kapasitas: 55,
    ukuran: "32m × 64m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Conference_table.jpg/320px-Conference_table.jpg"
  },
  {
    id: 5,
    kelas: "Melati",
    pendingin: true,
    proyektor: 1,
    kapasitas: 45,
    ukuran: "32m × 32m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/NDCH_Schwerin.jpg/640px-NDCH_Schwerin.jpg"
  },
  {
    id: 6,
    kelas: "Melati",
    pendingin: true,
    proyektor: 2,
    kapasitas: 100,
    ukuran: "100m × 100m",
    gambar: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/WM2006_0018.jpg/640px-WM2006_0018.jpg"
  },
  {
    id: 7,
    kelas: "Melati",
    pendingin: true,
    proyektor: 1,
    kapasitas: 12,
    ukuran: "10m × 10m",
    gambar: "https://arsitagx-master.s3.ap-southeast-1.amazonaws.com/img_large/2761/3497/24544/photo-meeting-room-schaeffler-bearings-indonesia-desain-arsitek-oleh-pt-asa-adiguna.jpeg"
  },
  {
    id: 8,
    kelas: "Mawar",
    pendingin: true,
    proyektor: 1,
    kapasitas: 10,
    ukuran: "10m × 10m",
    gambar: "http://furnizing.com/files/img/5a72939d6dcae.jpg"
  },
  {
    id: 9,
    kelas: "Mawar",
    pendingin: true,
    proyektor: 1,
    kapasitas: 10,
    ukuran: "15m × 15m",
    gambar: "http://furnizing.com/files/img/5a72942ec8f93.jpg"
  },
  {
    id: 10,
    kelas: "Mawar",
    pendingin: true,
    proyektor: 1,
    kapasitas: 14,
    ukuran: "10m × 30m",
    gambar: "http://dizeen.id/wp-content/uploads/2020/05/interior-ruang-meeting-4.jpg"
  },
  {
    id: 11,
    kelas: "Mawar",
    pendingin: true,
    proyektor: 1,
    kapasitas: 10,
    ukuran: "10m × 10m",
    gambar: "http://furnizing.com/files/img/5a72939d6dcae.jpg"
  },
  {
    id: 12,
    kelas: "Mawar",
    pendingin: true,
    proyektor: 1,
    kapasitas: 12,
    ukuran: "10m × 30m",
    gambar: "http://dizeen.id/wp-content/uploads/2020/05/interior-ruang-meeting-minimalis-2.jpg"
  },
  {
    id: 13,
    kelas: "Aster",
    pendingin: true,
    proyektor: 1,
    kapasitas: 150,
    ukuran: "50m × 50m",
    gambar: "https://id.menarapeninsulahotel-jakarta.com/cache/06/b0/06b0a83e1a646f1c1f2435edbe28baea.jpg"
  },
  {
    id: 14,
    kelas: "Aster",
    pendingin: true,
    proyektor: 1,
    kapasitas: 20,
    ukuran: "10m × 30m",
    gambar: "https://id.menarapeninsulahotel-jakarta.com/cache/e3/be/e3beb64c80dd0978093b112b0986fcbf.jpg"
  },
  {
    id: 15,
    kelas: "Aster",
    pendingin: true,
    proyektor: 1,
    kapasitas: 30,
    ukuran: "10m × 30m",
    gambar: "https://id.menarapeninsulahotel-jakarta.com/cache/65/6c/656cfe8a830dd11918d0df45ca2d645f.jpg"
  },
];

export default rooms;
