import React, { Component } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';
import './App.css';
import About from './pages/About';
import DetailRuang from './pages/DetailRuang';
import Home from './pages/Home';
import Login from './pages/Login';



export default class App extends Component {
  render() {
    return (
      <Router>
        <Navbar bg="dark" variant="dark" expand="lg">
          <Container>
          <Navbar.Brand href="#home">Peminjaman Ruangan</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Link className="nav-link" to="/">Home</Link>
              <Link className="nav-link" to="/about">About</Link>
            </Nav>
          </Navbar.Collapse>
          </Container>
        </Navbar>

        <Switch>
          { /* Baca nilai URL dan render komponen yang benar */}
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path='/detail_ruang/:id'>
            <DetailRuang />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>

        <Container fluid className="container-footer mt-5">
          <p>UI-FE-07</p>
        </Container>
      </Router>
    )
  }
}
