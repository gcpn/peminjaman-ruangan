import React, { Component } from 'react';
import { Button, Container, Table } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
import rooms from '../data';

export default class Home extends Component {
    constructor(props) {
        super(props)
        
        // simpan daftar ruangan ke localstorage juga
        reactLocalStorage.setObject('availableRooms', { rooms: rooms });
        
        // buat array berisi <tr>
        this.rowRooms = rooms.map((item, index) => {
            return (
                <tr>
                    <td>{item.id + 1}</td>
                    <td>{item.kelas}</td>
                    <td>{item.pendingin ? "Ya" : "Tidak"}</td>
                    <td>{item.proyektor}</td>
                    <td>{item.kapasitas}</td>
                    <td><Link to={'/detail_ruang/' + index}><Button variant="primary" size="sm">Lihat Detail</Button></Link></td>
                </tr>
            )
        })
    }

    render() {
        if (!reactLocalStorage.get('isLoggedIn', false)) {
            return (<Redirect to="/login" />)
        }
        return (
            <Container className="my-3">
                <h1 className="mb-3">Daftar Ruangan</h1>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kelas Ruang</th>
                            <th>Pendingin AC</th>
                            <th>Proyektor (unit)</th>
                            <th>Kapasitas (orang)</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.rowRooms}
                    </tbody>
                </Table>
            </Container>
        )
    }
}