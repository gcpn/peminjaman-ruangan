import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import profile1 from "../assets/img/pp.jpg";
import profile2 from "../assets/img/pp1.jpg";
import profile3 from "../assets/img/pp2.jpg";
import profile4 from "../assets/img/pp3.jpg";

export default function About() {
  const teams = [
    {
      nama: "Gottfried Christophorus Prasetyadi",
      gambar: profile1,
      role: "Developer & Deployer",
    },
    {
      nama: "Muhammad Aswar Bakri",
      gambar: profile2,
      role: "Desainer",
    },
    {
      nama: "Abdurraziq Bachmid",
      gambar: profile3,
      role: "Developer",
    },
    {
      nama: "Muhamad Hilman",
      gambar: profile4,
      role: "Developer",
    },
  ];

  return (
    <Container>
      <h1 className="mt-5 text-center">
        Peminjaman Ruangan Rapat<br/>Perpustakaan Aksara Kami
      </h1>
      <h2 className="mt-5 text-dark">About</h2>
      <hr />
      <p>
        Aplikasi <b>Peminjaman Ruangan Rapat</b> ini dibuat dengan menggunakan
        ReactJS, nodeJS serta beberapa tools lainnya yang di-<i>depeloy</i>{" "}
        dengan menggunakan layanan E2C AWS.
      </p>

      <h2 className="mt-5 text-dark">Team</h2>
      <hr />
      <p>Aplikasi sederhana ini dibuat oleh:</p>

      <Row style={{ maxWidth: "960px" }} className="mx-auto">
        {teams.map(({ nama, gambar, role }, index) => (
          <Col key={index} md={6} lg={4} className="mt-4">
            <Row>
              <Col xs={4}>
                <img
                  src={gambar}
                  className="img-fluid rounded-circle"
                  alt="img"
                />
              </Col>
              <Col xs={8}>
                <h5 className="text-dark">{nama}</h5>
                <h6 className="text-secondary">{role}</h6>
              </Col>
            </Row>
          </Col>
        ))}
      </Row>

      <h2 className="mt-5 text-dark">Disclaimer</h2>
      <hr />
      <p>
        Penggunaan gambar-gambar dalam Aplikasi ini hanya bertujuan untuk
        edukasi. Pengembang tidak mencari keuntungan sedikitpun dari penggunaan
        gambar-gambar tersebut.
      </p>
      <p>
        Hak cipta dari semua gambar yang ditampilkan dalam halaman ini
        sepenuhnya merupakan hak pemiliknya masing-masing.
      </p>
    </Container>
  );
}
