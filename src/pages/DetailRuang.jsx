import React, { Component } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { reactLocalStorage } from "reactjs-localstorage";
import "./DetailRuang.css";

class DetailRuang extends Component {
    constructor(props) {
        super(props);
        const ruang_id = parseInt(this.props.match.params.id);
        // query ruang dengan id :id dari availableRooms localstorage
        this.state = {
            ruang: reactLocalStorage
                .getObject("availableRooms")
                .rooms.find((room) => room.id === ruang_id), // ruang adalah sebuah Object
            tanggal: "1970-1-1",
            jam_mulai: "00:00",
            jam_selesai: "00:00",
            tujuan_penggunaan: "",
        };

        // binding
        this.orderRuangan = this.orderRuangan.bind(this);
        this.setTanggal = this.setTanggal.bind(this);
        this.setJamMulai = this.setJamMulai.bind(this);
        this.setJamSelesai = this.setJamSelesai.bind(this);
        this.setTujuan = this.setTujuan.bind(this);
    }

    orderRuangan(event) {
        event.preventDefault()
        // update 'orderedRooms' di localstorage
        var orderedRooms = reactLocalStorage.get("orderedRooms", "");
        orderedRooms = orderedRooms + this.state.ruang.id + ",";
        reactLocalStorage.set("orderedRooms", orderedRooms);
        this.forceUpdate()
    }

    setTanggal(event) {
        this.setState({ tanggal: event.target.value });
    }

    setJamMulai(event) {
        this.setState({ jam_mulai: event.target.value });
    }

    setJamSelesai(event) {
        this.setState({ jam_selesai: event.target.value });
    }

    setTujuan(event) {
        this.setState({ tujuan: event.target.value });
    }

    renderForm() {
        var orderedRooms = reactLocalStorage.get("orderedRooms", "");
        console.log(orderedRooms.split(",").includes(this.state.ruang.id));
        if (
            orderedRooms !== "" &&
            orderedRooms.split(",").includes(this.state.ruang.id.toString())
        ) {
            return (
                <Container>
                    <h2 className="my-3">Status</h2>
                    <p>Anda sudah memesan ruangan ini</p>
                    <p>Status: menunggu persetujuan</p>
                </Container>
            );
        } else {
            return (
                <Container>
                    <h2 className="my-3">Form Peminjaman</h2>
                    <Form className="form-pesanruangan">
                        <Form.Group>
                            <Form.Label>Tanggal</Form.Label>
                            <Form.Control type="date" onChange={this.setTanggal} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Jam Mulai</Form.Label>
                            <Form.Control type="time" onChange={this.setJamMulai} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Jam Selesai</Form.Label>
                            <Form.Control type="time" onChange={this.setJamSelesai} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Tujuan Penggunaan</Form.Label>
                            <Form.Control as="textarea" rows={5} onChange={this.setTujuan} />
                        </Form.Group>
                        <Button onClick={this.orderRuangan} variant="primary" type="submit">
                            Pesan Ruangan
            </Button>
                    </Form>
                </Container>
            );
        }
    }

    render() {
        return (
            <Container>
                <h1 className="my-4">Ruang {this.state.ruang.id + 1}</h1>

                <div className="row">
                    <div className="col-md-8">
                        <img className="w-100 gbr-rg" alt="gambar-ruangan" src={this.state.ruang.gambar} />
                    </div>

                    <div className="col-md-4 mt-2">
                        <dl>
                            <dt>Kelas</dt>
                            <dd>{this.state.ruang.kelas}</dd>

                            <dt>Pendingin AC</dt>
                            <dd>{this.state.ruang.pendingin ? "Ada" : "Tidak Ada"}</dd>

                            <dt>Jumlah Proyektor</dt>
                            <dd>{this.state.ruang.proyektor}</dd>

                            <dt>Kapasitas</dt>
                            <dd>{this.state.ruang.kapasitas + " orang"}</dd>

                            <dt>Ukuran Ruang</dt>
                            <dd>{this.state.ruang.ukuran}</dd>
                        </dl>
                    </div>
                </div>
                <hr />
                {this.renderForm()}
            </Container>
        );
    }
}

export default withRouter(DetailRuang);
