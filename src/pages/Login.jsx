import React, { Component } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import { Redirect } from 'react-router-dom';
import { reactLocalStorage } from 'reactjs-localstorage';
import './Login.css'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoggedIn: reactLocalStorage.get('isLoggedIn', false)
        }
        this.authLogin = this.authLogin.bind(this)
    }
    
    // semua email/pass OK
    authLogin(event) {
        event.preventDefault() // cegah submisi form karena tidak perlu
        this.setState({ isLoggedIn: true })
        reactLocalStorage.set('isLoggedIn', true)
    }
    
    render() {
        if (this.state.isLoggedIn) {
            return <Redirect to="/" />
        }
        return (
            <Container className="my-3">
                <Form className="form-login mx-auto">
                    <h2 className="mb-3">Silakan Login</h2>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control onChange={()=>{}} type="email" placeholder="user@domain.com" />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="masukkan password Anda" />
                    </Form.Group>
                    <Button onClick={this.authLogin} variant="primary" type="submit" className="mt-3">Login</Button>
                </Form>
            </Container>
        )
    }
}
